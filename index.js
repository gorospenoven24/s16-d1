//  while loop
/* - while loop takes in an expression/ conditon, if the condion evaluates to true, the statement inside the codes block will be executed

syntax -
	while(expression/condtion){
	statements;
	}

*/
let	decrement = 5;

	while(decrement >=0){
		console.log("while: " + decrement);
		decrement--;

	}


console.log(" increment while loop")

	let	count = 0;
	while(count <= 9){
		console.log("while: " + count);
		count++;

	}

	/*do while loop
		- a do while loop works a lot like  the while loop. but unlike the while loop, do while loops guarantee that the code will exceted at least once

		syntaxt-
			do {
				statement;
			}while(expression/ condition)
		*/


let number = Number(prompt("Give me a number"));

do{
	console.log("Do while" + number);
	number += 1;
}while(number <= 10)

/*
for loop 
-	for loop is more flexible that do whileloops. it consistof three parts:
1.Inialization - a value that will track the progression of the loop
2. Expression
3. Final expression
*/

console.log("for loop");

for (let count3 = 0; count3 <=3; count3++){
	console.log(count3);
}



console.log("another loop the lenght counting")
let myString = "alex";
console.log(typeof myString.length);

//  array loop

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

// word loop

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

console.log("vowel count")
let myName = "Alex";
for(let i= 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 

		){
		console.log(3);
	}
	else{
		console.log(myName[i])
	}
}


// continue and break statement
/* continue - a statement taht allos the code to go to the next iteration of the loop without finishing the execution of all statement in a code block;
	break- a statemnet taht is use to terminate the current loop oncce a match been found

*/

for (let count4 = 0; count4 <=20; count4++){
	if (count4 % 2 === 0){
	continue;
	}
console.log("continue and break: " + count4);
if(count4 > 10){
	break;
	}
}




console.log("Iterating  the length of string")

let name = "alexandro";
for( let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("continue to the next iteration");
		continue;
	}
	if (name[i] === "d"){
		break;
	}
}